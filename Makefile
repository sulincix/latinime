SDK=~/Android/Sdk
TARGET=28
TOOL=28.0.3
JAVADIR=$(JAVA_HOME)/bin
BUILDTOOLS=$(SDK)/build-tools/$(TOOL)
AJAR=$(SDK)/platforms/android-$(TARGET)/android.jar
ADX=$(BUILDTOOLS)/dx
AAPT=$(BUILDTOOLS)/aapt
JAVAC=$(JAVADIR)/javac
JAR=$(JAVADIR)/jar
JFLAGS=-source 8 -encoding utf-8
JARSIGNER=$(JAVADIR)/jarsigner
APKSIGNER=$(BUILDTOOLS)/apksigner
ZIPALIGN=$(BUILDTOOLS)/zipalign
KEYTOOL=$(JAVADIR)/keytool
ADB=$(SDK)/platform-tools/adb

KEYFILE=keystore.jks

SRC=src/
NAME=app


all: clear build addjar dx zipalign sign
build:
	mkdir bin
	mkdir gen
	mkdir assets src res || true
	$(AAPT) package -v -f -I $(AJAR) -M AndroidManifest.xml -A assets -S res -m -J gen -F bin/resources.ap_
	$(JAVAC) $(JFLAGS) -classpath "$(AJAR):jar/jsr305-3.0.2.jar:jar/android-support-v4.jar" -sourcepath $(SRC) -sourcepath gen -d bin $(shell find $(SRC) -name *.java)
	mv bin/resources.ap_ bin/$(NAME).ap_
zipalign:
	$(ZIPALIGN) -v -p 4 bin/$(NAME).ap_ bin/$(NAME)-aligned.ap_
	mv bin/$(NAME)-aligned.ap_ bin/$(NAME).ap_
optimize:
	optipng -o7 $(shell find res -name *.png)
sign:
	$(APKSIGNER) sign --ks $(KEYFILE) --out bin/$(NAME).apk bin/$(NAME).ap_
	rm -f bin/$(NAME).ap_
clear:
	rm -rf bin gen
install:
	$(ADB) install -r bin/$(NAME).apk
dx:
	$(ADX) --dex --output=bin/classes.dex bin
	cd bin ; $(AAPT) add $(NAME).ap_ classes.dex
	cp -prfv lib/ bin/
	cd bin; $(AAPT) add $(NAME).ap_ $(shell find lib -name *.so)
addjar:
	cd bin ; $(JAR) -xf ../jar/android-support-v4.jar ; rm -rf META-INF
	cd bin ; $(JAR) -xf ../jar/jsr305-3.0.2.jar ; rm -rf META-INF
	